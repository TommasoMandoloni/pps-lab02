package u02

object HelloScala extends App{

  // Functions

  def parity(x: Int): String = x match {
    case x if x % 2 == 0 => "even"
    case _ => "odd"
  }

  val par: Int => String = {
    case x if x % 2 == 0 => "even"
    case _ => "odd"
  }

  def negative(predicate: String => Boolean):String => Boolean = string => !predicate(string)

  val neg: (String => Boolean) => String => Boolean = f => s => !f(s)

  def genericNeg[T](predicate: T => Boolean):T => Boolean = string => !predicate(string)

  // Currying

  val p1: Double => Double => Double => Boolean = x => y => z => (x <= y) && (y <= z)

  val p2 = (x:Double, y:Double, z:Double) => (x <= y) && (y <= z)

  def p3(x:Double)(y:Double)(z:Double): Boolean = x match {
    case x if x <= y => y <= z
    case _ => false
  }

  def p4(x:Double, y:Double, z:Double): Boolean = (x <= y) && (y <= z)

  // Compose

  def compose(f:Int => Int, g:Int => Int):Int => Int = {
    x => f(g(x))
  }

  def genericComp[T](f:T => T, g:T => T): T => T = {
    x => f(g(x))
  }

  // Recursion

  def fib(n:Int):Int = n match{
    case 0 => 0
    case 1 | 2 => 1
    case _ => fib(n-1) + fib(n-2)
  }

  // Sum Types, Product Types, Modules

  sealed trait Shape
  object Shape {
    case class Rectangle(base:Double, height:Double) extends Shape
    case class Circle(radius:Double) extends Shape
    case class Square(side:Double) extends Shape

    def area(shape:Shape):Double = shape match {
      case Rectangle(b, h) => b * h
      case Circle(r) => (r * r) * 3.14
      case Square(s) => s * s
    }

    def perimeter(shape:Shape):Double = shape match {
      case Rectangle(b, h) => (b + h) * 2
      case Circle(r) => (2 * r) * 3.14
      case Square(s) => s * 4
    }

  }

  // Optionals

  sealed trait Option[A] // An Optional data type
  object Option {
    case class None[A]() extends Option[A]
    case class Some[A](a: A) extends Option[A]

    def filter[A](opt: Option[A])(predicate:A => Boolean): Option[A] = opt match {
      case Some(x) if predicate(x) => Some(x)
      case _ => None()
    }

    def map[A, B](opt: Option[A])(function: A => B): Option[B] = opt match {
      case Some(v) => Some(function(v))
      case _ => None()
    }

    // Union of two Option of the same type
    def map2[A](opt1: Option[A], opt2: Option[A])(function: (A, A) => A): Option[A] = (opt1, opt2) match {
      case (Some(x), Some(y)) => Some(function(x, y))
      case _ => None()
    }

  }

}
