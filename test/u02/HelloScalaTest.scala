package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.HelloScala._

class HelloScalaTest {

  @Test def testEven(){
    assertEquals("even", parity(2))
  }

  @Test def testOdd(){
    assertEquals("odd", parity(1))
  }

  @Test def testNegative(){
    val empty: String => Boolean = _ == ""
    val notEmpty = negative(empty)
    assertTrue(notEmpty("foo"))
    assertFalse(notEmpty(""))
    assertTrue(notEmpty("foo") && !notEmpty(""))
  }

  @Test def testGenerics(){
    val even: Int => Boolean = _ % 2 == 0
    val odd = genericNeg(even)
    assertTrue(odd(3))
    assertFalse(odd(4))
  }

  @Test def testCurrying(){
    assertTrue(p3(3)(4)(5))
    assertFalse(p4(3, 6, 5))
  }

  @Test def testCompose(){
    assertEquals(9, compose(_-1,_*2)(5))
    assertEquals(7.5, genericComp((n:Double) => n*0.5, (n:Double) => n+5)(10))
  }

  @Test def testFibonacci(){
    assertEquals(0, fib(0))
    assertEquals(1, fib(1))
    assertEquals(1, fib(2))
    assertEquals(2, fib(3))
    assertEquals(3, fib(4))
    assertEquals(5, fib(5))
  }

  @Test def testShape(){
    import u02.HelloScala.Shape._
    assertEquals(20, area(Rectangle(5,4)))
    assertEquals(18, perimeter(Rectangle(5,4)))
    assertEquals(25, area(Square(5)))
    assertEquals(20, perimeter(Square(5)))
    assertEquals(78.5, area(Circle(5)))
    assertEquals(6.28, perimeter(Circle(1)))
  }

  @Test def testOptionals(){
    import u02.HelloScala.Option._
    assertEquals(Some(5), filter(Some(5))(_ > 2))
    assertEquals(None(), filter(Some(5))(_ > 8))
    assertEquals(Some(true), map(Some(5))(_ > 2))
    assertEquals(Some(false), map(Some(5))(_ > 8))
    assertEquals(None(), map(None[Int])(_ > 2))
    assertEquals(Some(7), map2(Some(5), Some(2))(_ + _))
    assertEquals(Some(10), map2(Some(5), Some(2))(_ * _))
    assertEquals(None(), map2(Some(5), None())(_ + _))
    assertEquals(None(), map2(None(), Some(2))(_ + _))
  }

}
